export default function ({ redirect }) {
  // Always fail authentication
  return redirect('/')
}
